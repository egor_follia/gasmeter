import time, cv2, os
import paho.mqtt.client as mqtt
import numpy as np
from glob import glob
from scipy import misc
from PIL import Image

# информация
broker_addr = 'mqtt.astersky.ru'
broker = '212.237.33.172'
broker_port = 8286
light_channel = 'cmnd/slampher/power'

# новый юзер

path = os.getcwd()
path_snaps = path + '/snaps'
path_dig = path + '/digits'

# для подключения к счетчику
def connect_to_counter():
    # настройка соединения
    client = mqtt.Client('Kalach')
    client.username_pw_set(username = 'utya_iot', password = 'iot[eqdfv')
    client.connect(broker_addr, broker_port, 60)

    # подключаемся к топику и посылаем 1
    client.subscribe(light_channel)
    client.publish(light_channel, 1)
    time.sleep(10)  # задержка

    # подключение потока
    cap = cv2.VideoCapture("rtsp://syno:5b279bc71850d9e90c1fd9fff2409acf@192.168.234.101:554/Sms=7.unicast");

    # делаем 3 скрина с задеркой 3 сек
    for i in range(0,3):
        ret, frame = cap.read()
        cv2.imwrite(path_snaps + '/{0}.png'.format(i), frame)
        time.sleep(5)

    # выключаем свет
    client.publish(light_channel, 0)

    # на вся
    cap.release()
    cv2.destroyAllWindows()

# функция для подготовки цифр
def crop_digits():
    # считываем все скрины
    images = [cv2.imread(screen, 1) for screen in glob(path_snaps + '/*.png')]
    #image = cv2.imread('/home/follia/Documents/Number_recognition/Resources/snaps/1.png', 1)
    iter = 1

    # запускаем цикл, где отдельно каждый скрин обрабатывается
    for image in images:
        h, w, k = image.shape
        # форма картинки (высота, ширина)
        M = cv2.getRotationMatrix2D((w/2, h/2), 15, 1) # берем центр и вращаем на 15 градусов
        image = cv2.warpAffine(image, M, (w,h)) # применяем вращение к нашей картинке

        # обрезаем
        image = image[400:800, int((w-h)):int(h+(w-h)/2)]
        h, w, k = image.shape

        # подготовка картинки
        image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY) # перевод в чб
        threshold = 72
        cv2.threshold(image,threshold,255,cv2.THRESH_BINARY,image) #применяет уровень threshold к каждому пикселю
        cv2.bitwise_not(image,image)  # хрен знает, но она улучшает качество чб (можно делать and, or вместо not)
        im2, contours, hier = cv2.findContours(image, cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE) # делаем контура для обрезки

        # обрезка самой области с цифрами
        if len(contours) != 0:
            c = max(contours, key = cv2.contourArea)
            x,y,w,h = cv2.boundingRect(c)
            cv2.rectangle(image,(x,y),(x+w,y+h),(0,255,0),2)
            image = image[y:y + h, x:x + w]

        # делим на 5 частей
        h, w = image.shape
        for i in range(1, 6):
            digit = image[10:40, int((i - 1) * w / 9): int((i * w) / 9)] # разрезает

            # влияет на качество, можно играться с последними двумя цифрами
            digit_bin = cv2.adaptiveThreshold(digit,255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 31, 2)

            # все ниже, как написано на Хабре, для удаления шумов, но с ними особо не игрался
            kernel = np.ones((2,2),np.uint8)
            digit_bin = cv2.morphologyEx(digit_bin, cv2.MORPH_OPEN, kernel)
            # кроме этого, здесь можно вместо not ставить or, and и еще какие-то варианты, но это самый оптимальный
            digit_bin = cv2.bitwise_not(digit_bin,digit_bin)
            digit_bin = cv2.threshold(digit_bin, 100, 255, cv2.THRESH_BINARY)[1]

            #cv2.imshow("Result", digit_bin) # показывает результат
            # далее ресайз на 50х50 и сохраняем
            digit_bin = cv2.resize(digit_bin, dsize = (50,50), interpolation = cv2.INTER_CUBIC)
            cv2.imwrite(path_dig + '/{0}.png'.format(iter), digit_bin)
            #cv2.waitKey(0)

            iter += 1

if __name__ == '__main__':
    connect_to_counter()

    # ждем 3 секунды и запускаем обрезалку
    time.sleep(3)
    crop_digits()

    time.sleep(3)
    from kNN_predict import *

    # предсказываем
    time.sleep(3)
    i=0
    while i<len(images):
        if i == 0:
            txt.write('Screen 1\n')
        if i == 5:
            txt.write('\nScreen 2\n')
        if i == 10:
            txt.write('\nScreen 3\n')

        pred = predict(feature_extraction(images[i]))
        txt.write(str(pred[0]))
        i = i + 1

