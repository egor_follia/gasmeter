# this file to make snaps from camera
import vlc, time
import paho.mqtt.client as mqtt

# broker info
broker_addr = 'mqtt.astersky.ru'
broker = '212.237.33.172'
broker_port = 8286
light_channel = 'cmnd/slampher/power'

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

def connect_to_counter():
    # create new instance
    client = mqtt.Client('Kalach')

    # set up connection
    client.connect(broker_addr, broker_port, 60)
    client.on_connect = on_connect

    # send 1 to the topic - light on
    client.subscribe(light_channel)
    client.publish(light_channel, 1)
    time.sleep(5)  # delay 3 seconds

    # set rtsp connection
    i = vlc.Instance('--verbose 2'.split())
    player = i.media_player_new()

    # set address of rtsp stream
    stream = player.set_mrl('rtsp://syno:5b279bc71850d9e90c1fd9fff2409acf@192.168.234.101:554/Sms=7.unicast')
    player.set_media(stream)
    player.play()

    # make 3 screenshots
    for i in range(0,3):
        time.sleep(2)
        player.video_take_snapshot(0, '/home/follia/Documents/Gasmeter/snaps/{0}.png'.format(i), 0, 0)

    # send 0 to the topic - light off
    client.publish(light_channel, 0)

connect_to_counter()
