import numpy as np
import os
import scipy.ndimage
from skimage.feature import hog
from skimage import data, color, exposure
from sklearn.model_selection import  train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.externals import joblib
from skimage.feature import hog

features_list = []
features_label = []

path = os.getcwd()

# load labels as name of folder and feautres as images 50x50 to train
for digit in range(0,10):
    label = digit
    training_directory = path + '/train_set/' + str(label) + '/'
    for filename in os.listdir(training_directory):
    	if (filename.endswith('.png')):
    	    training_digit_image = scipy.misc.imread(training_directory + filename)
    	    df = hog(training_digit_image, orientations=8, pixels_per_cell=(10,10), cells_per_block=(5, 5))
    	    features_list.append(df)
    	    features_label.append(label)

features  = np.array(features_list, 'float64')
# split the labled dataset into training / test sets
X_train, X_test, y_train, y_test = train_test_split(features, features_label)
# train using K-NN
knn = KNeighborsClassifier(n_neighbors=3)
knn.fit(X_train, y_train)
# get the model accuracy
model_score = knn.score(X_test, y_test)

# save trained model
joblib.dump(knn, path + '/knn_model.pkl')
