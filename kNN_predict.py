import numpy as np
import os
from glob import glob
import scipy.ndimage
from skimage.feature import hog
from skimage import data, color, exposure
from sklearn.model_selection import  train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.externals import joblib
from skimage.feature import hog
from scipy import misc
from PIL import Image

path = os.getcwd()

# загружаем натренированную модель
knn = joblib.load(path + '/knn_model.pkl')

def feature_extraction(image):
    return hog(image, orientations=8, pixels_per_cell=(10, 10), cells_per_block=(5, 5))

def predict(df):
    predict = knn.predict(df.reshape(1,-1))[0]
    predict_proba = knn.predict_proba(df.reshape(1,-1))
    return predict, predict_proba[0][predict]

digits = []

# extract featuress
hogs = list(map(lambda x: feature_extraction(x), digits))
# apply k-NN model created in previous
predictions = list(map(lambda x: predict(x), hogs))

# делаем массив (лист) с фотками
images = []
for i in range(1,16):
    img = Image.open(path + '/digits/{0}.png'.format(i))
    images.append(img)

# файл для записи значений с фоток
txt = open(path + '/digits_for_send.txt', 'w+')

'''
# предсказываем
for img in images:
    pred = predict(feature_extraction(img))
    txt.write(str(pred[0]))
    #print(img.filename, ' -> ', pred[0])
'''
